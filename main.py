import telebot
from telebot import types
from decouple import config
from bs4 import BeautifulSoup as BS
import requests

bot = telebot.TeleBot(config('BOT_TOKEN'))
courts_api = None
massive_address = []


@bot.message_handler(commands=['help'])
@bot.message_handler(commands=['start'])
def start(message):
    mess = f'Привет, <b>{message.from_user.first_name} <u>{message.from_user.last_name}</u></b>\n' \
           f'Выберите нужный раздел в меню'
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2, one_time_keyboard=True)
    interactive_court_map = types.KeyboardButton('🗺️ Интерактивная карта')
    find_a_case = types.KeyboardButton('📂 Найти дело')
    claim_templates = types.KeyboardButton('📋 Шаблоны исков')
    markup.add(interactive_court_map)
    markup.add(find_a_case, claim_templates)
    bot.send_message(message.chat.id, mess, parse_mode='html', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call):
    global courts_api
    keyboard = types.InlineKeyboardMarkup(row_width=1)
    if call.data == "main_menu":
        call.message.text = "⏏ Главное меню"
        work_with_buttons(call.message)

    elif call.data == "court":
        requests.packages.urllib3.disable_warnings()
        response = requests.get('https://portal.sot.kg/api/v1/all/court/info/', verify=False)
        courts_api = response.json()
        courtRegion = []
        for x in courts_api:
            if x['courtRegionName'] in courtRegion:
                continue
            courtRegion.append(x['courtRegionName'])
            keyboard.add(types.InlineKeyboardButton(text=x['courtRegionName'], callback_data=f"{x['courtRegionId']}"))
        back = types.InlineKeyboardButton(text="⏪ Назад", callback_data='back_1')
        main_menu = types.InlineKeyboardButton(text="⏏ Главное меню", callback_data='main_menu')
        keyboard.add(back, main_menu)
        bot.edit_message_text("Выберите регион:", reply_markup=keyboard, chat_id=call.message.chat.id,
                              message_id=call.message.message_id)
    elif call.data == "back_1":
        call.message.text = '🗺️ Интерактивная карта'
        work_with_buttons(call.message)

    elif call.data == "back_2":
        call.data = 'court'
        callback_worker(call)

    elif call.data == "pssi":
        pass
    elif call.data == "supreme_court":
        bot.send_message(call.message.chat.id, f'<b>Верховный суд Кыргызской Республики </b>\n'
                                               f'Адрес: 720040, г. Бишкек, ул. Абдумомунова, 205\n'
                                               f'E-mail: vskr@sot.kg\nСайт: sot.kg\n'
                                               f'Время работы: 8:30 - 17:30\n'
                                               f'Время обеда: 12:00 - 13:00', parse_mode='html')
        bot.send_location(call.message.chat.id, 42.8796101, 74.6055496)

    elif call.data == "advocacy":
        mas = parsing(1)
        i = 0
        while i < len(mas):
            key = types.InlineKeyboardButton(text=mas[i], callback_data=f'advocacy{i}')
            keyboard.add(key)
            i += 1
        keyboard.add(types.InlineKeyboardButton(text="⏪ Назад", callback_data='back_1'))
        keyboard.add(types.InlineKeyboardButton(text="⏏ Главное меню", callback_data='main_menu'))
        bot.edit_message_text("Выберите адвокатуру:", reply_markup=keyboard, chat_id=call.message.chat.id,
                              message_id=call.message.message_id)
    elif massive_address:
        i = 0
        while i < len(massive_address):
            if call.data == f'advocacy{i}':
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
                main_menu = types.KeyboardButton("⏏ Главное меню")
                markup.add(main_menu)
                bot.send_message(call.message.chat.id, massive_address[i], parse_mode='html', reply_markup=markup)
            i += 1
    elif len(courts_api):
        i = 0
        for x in courts_api:
            if call.data == f"{x['courtRegionId']}":
                keyboard.add(types.InlineKeyboardButton(text=x['name'], callback_data=f"{x['mobNum']}"))
                i = 1
            elif call.data == f"{x['mobNum']}":  # callback_data
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
                main_menu = types.KeyboardButton("⏏ Главное меню")
                markup.add(main_menu)
                bot.send_message(call.message.chat.id, f'<b>{x["name"]}</b>\nАдрес: {x["address"]}\n',
                                 reply_markup=markup, parse_mode='html')
                for room in x["rooms"]:
                    bot.send_message(call.message.chat.id,
                                     f'Название: <b>{room["roomName"]}</b>\n'
                                     f'Тип зала: {room["roomTypeName"]}\n'
                                     f'Статус зала: {room["roomStatusName"]}\n'
                                     f'Оснащён подсистемами: {room["roomEquipment"]}',
                                     parse_mode='html')
                # bot.send_location(call.message.chat.id, 42.8776677, 74.5725763)
                message = 'https://www.google.com/maps/search/' + f'{x["name"]}'.replace(' ', '+')
                keyboard.add(types.InlineKeyboardButton(text="Перейти -->", url=message))
                bot.send_message(call.message.chat.id, f'<b>Посмотреть на карте</b>', reply_markup=keyboard,
                                 parse_mode='html')
                i = 0
        if i:
            back = types.InlineKeyboardButton(text="⏪ Назад", callback_data='back_2')
            main_menu = types.InlineKeyboardButton(text="⏏ Главное меню", callback_data='main_menu')
            keyboard.add(back, main_menu)
            bot.edit_message_text("Выберите суд:", reply_markup=keyboard, chat_id=call.message.chat.id,
                                  message_id=call.message.message_id)


def parsing(k):
    response = requests.get('http://advokatura.kg/kontakty/')
    html = BS(response.content, 'html.parser')
    convert = html.find("table", {"class": "contacts-table"}).find('tbody')  # Ищем какой именно блок нам нужен
    i = 0
    mas = []
    mass = []
    global massive_address
    for date in convert:
        x = date.findAll('td')
        for y in x:
            data = y.text.replace("\t", "").replace("\n\n", "\n").replace("Территориальная адвокатура", "ТА")
            if i % 2 == 0:
                mas.append(data)
            else:
                mass.append(data)
            i += 1
    if k == 1:
        massive_address = mass
        return mas


@bot.message_handler(content_types=['text'])
def work_with_buttons(message):
    if message.chat.type == 'private':
        if message.text == '🗺️ Интерактивная карта':
            keyboard = types.InlineKeyboardMarkup(row_width=1)
            supreme_court = types.InlineKeyboardButton(text='⚖️Верховный суд', callback_data='supreme_court')
            courts = types.InlineKeyboardButton(text='🧑‍⚖️Суды КР', callback_data='court')
            pssi = types.InlineKeyboardButton(text='👩‍⚖️ПССИ КР', callback_data='pssi')
            advocacy = types.InlineKeyboardButton(text='🕵️‍♂️Адвокатура КР', callback_data='advocacy')
            main_menu = types.InlineKeyboardButton(text="⏏ Главное меню", callback_data='main_menu')
            keyboard.add(supreme_court, courts, pssi, advocacy, main_menu)
            bot.send_message(message.chat.id, "Выберите тип:\n\n⬇️ ⬇️ ⬇️ ⬇️ ⬇️", parse_mode='html',
                             reply_markup=keyboard)

        elif message.text == '📂 Найти дело':
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            criminal_case = types.KeyboardButton('Уголовное дело')
            civil_case = types.KeyboardButton('Гражданское дело')
            administrative_case = types.KeyboardButton('Административное дело')
            markup.add(criminal_case, civil_case, administrative_case)
            bot.send_message(message.chat.id, "Выберите тип Вашего дела", parse_mode='html', reply_markup=markup)
        elif message.text == 'Уголовное дело':
            bot.send_message(message.from_user.id, "Напишите номер Вашего дела")
            bot.register_next_step_handler(message, get_number)
        elif message.text == "⏏ Главное меню":
            mess = f'Выберите нужный раздел в меню'
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2, one_time_keyboard=True)
            interactive_court_map = types.KeyboardButton('🗺️ Интерактивная карта')
            find_a_case = types.KeyboardButton('📂 Найти дело')
            claim_templates = types.KeyboardButton('📋 Шаблоны исков')
            markup.add(interactive_court_map)
            markup.add(find_a_case, claim_templates)
            bot.send_message(message.chat.id, mess, parse_mode='html', reply_markup=markup)
        elif message.text == '📋 Шаблоны исков':
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton('Скачать',
                                                  url='https://www.google.com/url?sa=i&url=https%3A%2F%2Fmadewithlar'
                                                      'avel.com%''2Ftelebot&psig=AOvVaw0MZkCGj_nBxHEhIN74voY3&ust=16688'
                                                      '73055316000&source='
                                                      'images&cd=vfe&ved=0CBAQjRxqFwoTCMiIxt-KuPsCFQAAAAAdAAAAABAE'))
            bot.send_message(message.chat.id, 'О взыскании долга по расписке', reply_markup=markup)
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton('Скачать', url='https://ru.stackoverflow.com/'))
            bot.send_message(message.chat.id, 'О выдаче копии судебного акта', reply_markup=markup)


def get_number(message):
    number = message.text
    bot.send_message(message.chat.id, f'Ваше дело: {number}', parse_mode='html')

while True:
    try:
        bot.polling(none_stop=True)
    except Exception as e:
        print(e)

